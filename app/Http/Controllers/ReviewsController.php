<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class ReviewsController extends Controller
{
    // List all reviews
    public function index()
    {
        $content = Storage::disk('local')->get('reviews.json');
        $content = json_decode($content, true);
        return response()->json($content, 201);
    }
}
