<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class PhotosController extends Controller
{
    // List all photos
    public function index()
    {
        $content = Storage::disk('local')->get('photos.json');
        $content = json_decode($content, true);
        return response()->json($content, 201);
    }
}
